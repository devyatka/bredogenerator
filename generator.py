import argparse
import random
import statistics


def init_arguments():
    parser = argparse.ArgumentParser(prog='Бредогенератор',
                                     usage='generator.py -f filename [-s number of sentences]')
    parser.add_argument('-f', '--file', help='Файл со статистикой', default=None)
    parser.add_argument('-s', '--sentences', help='Количество предложений для генерации', default=5)
    return parser


def main():
    parser = init_arguments()

    if not statistics.good_argument(parser.parse_args().sentences, 1):
        print('Некорректное количество предложений')
        return
    if parser.parse_args().file is None:
        print('usage:', parser.usage)
        return

    try:
        ngrams = statistics.parse_stat(parser.parse_args().file)
        if ngrams is None:
            return
    except FileNotFoundError:
        print('Файл не найден')
        return

    ngrams_dict = make_ngrams_dict(ngrams)
    bred_text = generate_bred(int(parser.parse_args().sentences), ngrams_dict)
    file = open('bredText.txt', 'w', encoding='utf-8')
    file.write(bred_text)
    file.close()
    print(bred_text)


def generate_bred(sent_count, ngrams_dict):
    bred_text = ''
    for i in range(sent_count):
        rand_first_word = random.choice(list(ngrams_dict.keys()))
        bred_text += generate_one_sentence(ngrams_dict, rand_first_word)
    return bred_text


def make_ngrams_dict(ngrams_list):
    ngrams_dict = {}
    for words, count in ngrams_list:
        if not words[0] in ngrams_dict:
            ngrams_dict[words[0]] = []
        ngrams_dict[words[0]].append((words[1:], count))
    return ngrams_dict


def generate_one_sentence(ngrams_dict, current_word):
    sentence = make_capital_letter(current_word)
    while current_word in ngrams_dict.keys():
        words = ngrams_dict[current_word]
        next_words_tuple = words[random.randint(0, len(words) - 1)][0]
        for word in next_words_tuple:
            sentence += ' ' + word
        current_word = next_words_tuple[len(next_words_tuple) - 1]
    sentence += '. '
    return sentence


def make_capital_letter(word):
    return word[0].upper() + word[1:].lower()


if __name__ == '__main__':
    main()
