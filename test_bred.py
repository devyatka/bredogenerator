#!/usr/bin/env python3

import generator
import unittest
import text_parser
import statistics


class TestTextParser(unittest.TestCase):
    def test_parse_one_sentence(self):
        simple_sentence = 'Разбитое по словам предложение'
        simple_result = ['разбитое', 'по', 'словам', 'предложение']
        self.assertListEqual(text_parser.parse_one_sentence(simple_sentence), simple_result)

        symbols_sentence = 'Разные, *+=1234567890~&()[]{}" символы'
        symbols_result = ['разные,', 'символы']
        self.assertListEqual(text_parser.parse_one_sentence(symbols_sentence), symbols_result)

    def test_parse_sentences(self):
        simple_text = 'Простой тест. Три предложения! С разными знаками?'
        simple_result = [['простой', 'тест'], ['три', 'предложения'], ['с', 'разными', 'знаками']]
        self.assertListEqual(text_parser.parse_sentences(simple_text), simple_result)

        symbols_text = 'Разные, *+=1234567890~&()[]{}" символы'
        symbols_result = [['разные,', 'символы']]
        self.assertListEqual(text_parser.parse_sentences(symbols_text), symbols_result)

        unusual_text = 'Какие-то НЕоБыЧНыЕ СлОвА и don\'t'
        unusual_result = [['какие-то', 'необычные', 'слова', 'и', 'don\'t']]
        self.assertEqual(text_parser.parse_sentences(unusual_text), unusual_result)

    def test_make_ngramms(self):
        bigram_text = 'Постоить биграммы по двум предложениям. По двум простым предложениям'
        bigram_result = [(('по', 'двум'), 2), (('постоить', 'биграммы'), 1), (('биграммы', 'по'), 1),
                         (('двум', 'предложениям'), 1), (('двум', 'простым'), 1), (('простым', 'предложениям'), 1)]
        self.assertEqual(text_parser.make_ngramms(bigram_text, 2), bigram_result)

        trigram_text = 'Теперь триграммы. Теперь строим триграммы.'
        trigram_result = [(('теперь', 'строим', 'триграммы'), 1)]
        self.assertEqual(text_parser.make_ngramms(trigram_text, 3), trigram_result)

        fivegram_text = 'Простое предложение из пяти слов. Из четырех не подойдет!'
        fivegram_result = [(('простое', 'предложение', 'из', 'пяти', 'слов'), 1)]
        self.assertEqual(text_parser.make_ngramms(fivegram_text, 5), fivegram_result)

        diff_cases_text = 'сТРаННый текст, написАННЫЙ пО-РАзноМу'
        diff_cases_res = [(('странный', 'текст,'), 1), (('текст,', 'написанный'), 1), (('написанный', 'по-разному'), 1)]
        self.assertEqual(text_parser.make_ngramms(diff_cases_text, 2), diff_cases_res)


class TestGenerator(unittest.TestCase):
    def test_make_capital_letter(self):
        word = 'слово-с'
        result = 'Слово-с'
        self.assertEqual(generator.make_capital_letter(word), result)

        strange_word = 'КраКОзЯбрА'
        strange_result = 'Кракозябра'
        self.assertEqual(generator.make_capital_letter(strange_word), strange_result)

    def test_make_ngrams_dict(self):
        bigrams_list = [(('one', 'two-t'), 4), (('three', 'four'), 1)]
        bigrams_dict = {'one': [(('two-t',), 4)], 'three': [(('four',), 1)]}
        self.assertEqual(generator.make_ngrams_dict(bigrams_list), bigrams_dict)

        fivegrams_list = [(('a', 'b', 'c', 'd', 'e'), 7), (('e', 'f', 'g', 'h', 'i'), 5),
                          (('i', 'j', 'k', 'l', 'm'), 2)]
        fivegrams_dict = {'a': [(('b', 'c', 'd', 'e'), 7)], 'e': [(('f', 'g', 'h', 'i'), 5)],
                          'i': [(('j', 'k', 'l', 'm'), 2)]}
        self.assertEqual(generator.make_ngrams_dict(fivegrams_list), fivegrams_dict)

    def test_generate_one_sentence(self):
        birams_dict = {'one': [(('two-t',), 4)], 'three': [(('four',), 1)]}
        word = 'one'
        sentence = 'One two-t. '
        self.assertEqual(generator.generate_one_sentence(birams_dict, word), sentence)

        fivegrams_dict = {'a': [(('b', 'c', 'd', 'e'), 7)], 'e': [(('f', 'g', 'h', 'i'), 5)],
                          'i': [(('j', 'k', 'l', 'm'), 2)]}
        word = 'a'
        sentence = 'A b c d e f g h i j k l m. '
        self.assertEqual(generator.generate_one_sentence(fivegrams_dict, word), sentence)

    def test_generate_bred(self):
        bigrams_dict = {'one': [(('two-t',), 4)], 'two-t': [(('three',), 1)]}
        result = generator.generate_bred(1, bigrams_dict)
        good_result = result == 'One two-t three. ' or result == 'Two-t three. '
        self.assertTrue(good_result)

        trigrams_dict = {'a': [(('b', 'c'), 4)], 'b': [(('bla', 'blabla'), 3)], 'c': [(('d', 'e'), 1)]}
        result = generator.generate_bred(1, trigrams_dict)
        good_result = result == 'A b c d e. ' or result == 'B bla blabla. ' or result == 'C d e. '
        self.assertTrue(good_result)


class TestStatistics(unittest.TestCase):
    def test_ngram_to_string(self):
        bigram = (('one', 'two-three'), 10)
        str_bigram = '[ one two-three ] : 10'
        self.assertEqual(statistics.ngram_to_string(bigram), str_bigram)

        trigram = (('one', 'two', 'three'), 5)
        str_trigram = '[ one two three ] : 5'
        self.assertEqual(statistics.ngram_to_string(trigram), str_trigram)

    def test_write_down_in_file(self):
        bigrams = [(('one-t', 'two'), 2), (('three', 'four'), 1)]
        written_trigrams = '[ one-t two ] : 2\n[ three four ] : 1\n'
        statistics.write_down_in_file(bigrams)
        file = open('stat.txt', 'r', encoding='utf-8')
        text = file.read()
        file.close()
        self.assertEqual(text, written_trigrams)

    def test_parse_stat(self):
        file = open('testStat.txt', 'w', encoding='utf-8')
        file.write('[ one two-t ] : 2\n[ three four ] : 1\n')
        file.close()
        bigrams = [(('one', 'two-t'), 2), (('three', 'four'), 1)]
        self.assertEqual(statistics.parse_stat('testStat.txt'), bigrams)

        file = open('testStat.txt', 'w', encoding='utf-8')
        file.write(' [ триграммы в файле ] : 5\n[ записаны всяко-разно так ] : 4\n[ a b c ] : 1\n')
        file.close()
        trigrams = [(('триграммы', 'в', 'файле'), 5), (('записаны', 'всяко-разно', 'так'), 4), (('a', 'b', 'c'), 1)]
        self.assertEqual(statistics.parse_stat('testStat.txt'), trigrams)

        file = open('testStat.txt', 'w', encoding='utf-8')
        file.write('blablabla')
        file.close()
        self.assertIsNone(statistics.parse_stat('testStat.txt'))

    def test_good_argument(self):
        self.assertTrue(statistics.good_argument(5, 2))
        self.assertTrue(statistics.good_argument(-1, -4))
        self.assertFalse(statistics.good_argument(2, 5))
        self.assertFalse(statistics.good_argument(-3, -2))
        self.assertFalse(statistics.good_argument('qwer', 2))
        self.assertFalse(statistics.good_argument(1.4, 2))


if __name__ == "__main__":
    unittest.main()
